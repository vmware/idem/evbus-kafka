import uuid

import aiokafka.structs


async def test_libs(ctx, acct_profile, consumer: aiokafka.AIOKafkaConsumer):
    """
    Run the test without going through the hub to verify the way it works normally
    """

    key = b"my_key"
    message = str(uuid.uuid4()).encode()
    async with aiokafka.AIOKafkaProducer(**ctx.acct.connection) as producer:
        ack: aiokafka.structs.RecordMetadata = await producer.send_and_wait(
            topic=ctx.acct.topics[0], value=message, key=key
        )

        assert ack

        received_message: aiokafka.ConsumerRecord = await consumer.getone()

        assert received_message.topic == ctx.acct.topics[0]
        assert received_message.value == message
        assert received_message.key == key


async def test_publish_direct(
    hub, ctx, acct_profile, consumer: aiokafka.AIOKafkaConsumer
):
    """
    Run the test publishing directly to the kafka ingress plugin
    """
    message = str(uuid.uuid4())
    ctx.acct.key = None
    await hub.ingress.kafka.publish(ctx, body=message)

    received_message = await consumer.getone()

    assert received_message.topic == ctx.acct.topics[0]
    assert received_message.value == message.encode()
    assert received_message.key is None


async def test_publish_with_str_key(
    hub, ctx, acct_profile, consumer: aiokafka.AIOKafkaConsumer
):
    """
    Run the test publishing directly to the kafka ingress plugin
    """

    key = "my_key"
    ctx.acct.key = key
    message = str(uuid.uuid4())
    await hub.ingress.kafka.publish(ctx, body=message)

    received_message = await consumer.getone()

    assert received_message.topic == ctx.acct.topics[0]
    assert received_message.value == message.encode()
    assert received_message.key == key.encode()


async def test_publish_with_bytes_key(
    hub, ctx, acct_profile, consumer: aiokafka.AIOKafkaConsumer
):
    """
    Run the test publishing directly to the kafka ingress plugin
    """

    key = b"my_key"
    ctx.acct.key = key
    message = str(uuid.uuid4())
    await hub.ingress.kafka.publish(ctx, body=message)

    received_message = await consumer.getone()

    assert received_message.topic == ctx.acct.topics[0]
    assert received_message.value == message.encode()
    assert received_message.key == key


async def test_publish_indirect(
    hub, ctx, evbus_broker, acct_profile, consumer: aiokafka.AIOKafkaConsumer
):
    """
    Run the test by publishing to the evbus broker and waiting for it to show up in kafka
    """
    message = str(uuid.uuid4())
    ctx.acct.key = None

    # Put a message on the evbus broker, it should propagate to pika soon
    hub.evbus.broker.put_nowait(profile=acct_profile, body=message)
    await hub.pop.loop.sleep(0)

    received_message = await consumer.getone()

    assert received_message.topic == ctx.acct.topics[0]
    assert received_message.value == f'"{message}"'.encode()
    assert received_message.key == None
